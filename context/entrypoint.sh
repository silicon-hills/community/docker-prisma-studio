#!/bin/sh

# File: /entrypoint.sh
# Project: <<projectname>>
# File Created: 20-08-2021 15:19:07
# Author: Clay Risser <email@clayrisser.com>
# -----
# Last Modified: 20-08-2021 16:39:05
# Modified By: Clay Risser <email@clayrisser.com>
# -----
# Silicon Hills LLC (c) Copyright 2021
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export CHECKPOINT_DISABLE=1

if [ -z "$POSTGRES_URL" ]; then
  export POSTGRES_URL="postgresql://$POSTGRES_USERNAME:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DATABASE?schema=$POSTGRES_SCHEMA"
fi

cat <<EOF > schema.prisma
datasource db {
  provider = "postgresql"
  url      = env("POSTGRES_URL")
}
EOF
sleep 5
node_modules/.bin/prisma db pull --url $POSTGRES_URL
exec node_modules/.bin/prisma studio $@
